#include <avr/io.h>
#include <avr/interrupt.h>
#include "buttons.hpp"
#include "definitions.hpp"
#include "usart.hpp"

#define BUTTONS_ENABLED 0b00111100
#define POWER_BUTTON 0b00000100
#define MODE_BUTTON 0b00001000
#define PLUS_BUTTON 0b00010000
#define MINUS_BUTTON 0b00100000

uint8_t previousState = BUTTONS_ENABLED;
uint8_t pressedButtons = 0x00;
uint8_t forbiddenButtons = 0xFF;

ISR(PCINT2_vect) {
	if (PIND & BUTTONS_ENABLED) 
	{
		
	}
}

void setupButtons()
{
	DDRD |= ~BUTTONS_ENABLED & BUTTONS_ENABLED;
	PORTD |= BUTTONS_ENABLED;
}

inline void forbidButton(const uint8_t unpressedButtons)
{
	if (unpressedButtons)
	{
		forbiddenButtons = (~pressedButtons) & unpressedButtons;
	}
}

uint8_t processButtons()
{
	const uint8_t current_state = PIND | (~BUTTONS_ENABLED); // ��������� 0 - ������� ������
	const uint8_t unpressed_buttons = pressedButtons & current_state & forbiddenButtons;
	forbiddenButtons |= current_state;
	forbidButton(unpressed_buttons);
	uint8_t holdenButtons = pressedButtons & forbiddenButtons;
	pressedButtons = ~(current_state | previousState);
	previousState = current_state;
	
	if (pressedButtons == 0x00 && unpressed_buttons == 0x00)
	{
		return 0;
	}
	
	printf("pins - %i, current - %i, pressed - %i, unpressed - %i, forbidden - %i\n", PIND, current_state, pressedButtons, unpressed_buttons, forbiddenButtons);
	if (((pressedButtons & POWER_BUTTON) && (unpressed_buttons & MINUS_BUTTON)) || 
		((unpressed_buttons & POWER_BUTTON) && (pressedButtons & MINUS_BUTTON)))
		
	{
		return ENABLE_WAVE;
	}

	if (((pressedButtons & POWER_BUTTON) && (unpressed_buttons & PLUS_BUTTON)) ||
		((unpressed_buttons & POWER_BUTTON) && (pressedButtons & PLUS_BUTTON)))
	
	{
		return SAVE_MUSIC;
	}
	
	if (pressedButtons & POWER_BUTTON)
	{
		return 0;
	}
	
	if (unpressed_buttons & POWER_BUTTON)
	{
		return ON_OFF;
	}
	
	if (unpressed_buttons & MODE_BUTTON)
	{
		return CHANGE_MODE;
	}
	
	if (holdenButtons & PLUS_BUTTON)
	{
		return UP_BTN;
	}
		
	if (holdenButtons & MINUS_BUTTON)
	{
		return DOWN_BTN;
	}
	
	return 0;
}

