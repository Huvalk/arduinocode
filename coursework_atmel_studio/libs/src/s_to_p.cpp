#include <avr/io.h>
#include "s_to_p.hpp"
#include "usart.hpp"

#define THIS_PORT PORTC
#define THIS_DDR DDRC
#define THIS_PINS 0b00001110
#define DATA 1
#define CLEAR 2
#define SHIFT 3

void setupRegister()
{	
	THIS_DDR |= THIS_PINS;
	THIS_PORT &= ~THIS_PINS;
	THIS_PORT |= (1 << CLEAR);
	set();
}

void writeOne()
{
	THIS_PORT &= ~(1<<DATA);
	for (uint8_t i = 0; i < 7; ++i)
	{
		shift();
	}
	
	THIS_PORT |= (1<<DATA);
	shift();
	THIS_PORT &= ~(1<<DATA);
}

void shift()
{
	THIS_PORT |= (1 << SHIFT);
	THIS_PORT &= ~(1 << SHIFT);
}

void clear()
{
	THIS_PORT &= ~(1 << CLEAR);
	THIS_PORT |= (1 << CLEAR);
}

void set()
{
	THIS_PORT |= (1<<DATA);
	for (uint8_t i = 0; i < 8; ++i)
	{
		shift();
	}
	THIS_PORT &= ~(1<<DATA);
}
