#include <avr/eeprom.h>
#include "memory.hpp"
#include "usart.hpp"

#define INTERNAL_MEMORY_SIZE 0x03FF

#define LAST_STATE 0x0000

#define RED_COMPONENT 0x0003
#define BLUE_COMPONENT 0x0004
#define GREEN_COMPONENT 0x0005

void saveState(const uint8_t state)
{
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)LAST_STATE, state);
}

void saveColor(const uint8_t* const color)
{
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)RED_COMPONENT, color[0]);
	eeprom_write_byte((uint8_t*)GREEN_COMPONENT, color[1]);
	eeprom_write_byte((uint8_t*)BLUE_COMPONENT, color[2]);
}

void restoreSettings(uint8_t& state, uint8_t* const color)
{
	eeprom_busy_wait();
	state = eeprom_read_byte((uint8_t*)LAST_STATE);
	
	color[0] = eeprom_read_byte((uint8_t*)RED_COMPONENT);
	color[1] = eeprom_read_byte((uint8_t*)GREEN_COMPONENT);
	color[2] = eeprom_read_byte((uint8_t*)BLUE_COMPONENT);
	
	if (state > 12)
	{
		state = 1;
	}
	
	if (((color[0] == 0) && (color[1] == 0) && (color[2] == 0)) ||
		((color[0] > 0) && (color[1] > 0) && (color[2] > 0)))
	{
		color[0] = 255;
		color[1] = color[2] = 0;
	}
}

