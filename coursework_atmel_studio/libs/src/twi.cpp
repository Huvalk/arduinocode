#include <avr/io.h>
#include "twi.hpp"
#include "usart.hpp"

// �������� ��� �� ��������
#define get_bit(reg,bitnum) ((reg & (1<<bitnum))>>bitnum)

void TWI_Init()
{
	// 180 �������� ����
	TWSR = 0;
	// 400 ��� - 12, �� ��� ������ 200 ��� - 32
	TWBR = 12;

	TWCR |= 1<<TWEN;
}

void TWI_Start()
{
	TWCR= (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);

	while (get_bit(TWCR,TWINT)==0)
	{
	}
	
	uint8_t result = TWSR & 0b11111000;
	if (result != 0x08 && result != 0x10)
	{
		printf("start error: %x. ", result);
	}
}

void TWI_Stop()
{
	TWCR=(1<<TWSTO)|(1<<TWEN)|(1<<TWINT);
}

void TWI_Write (const uint8_t data)
{
	TWDR = data;
	TWCR = (1<<TWINT)|(1<<TWEN);

	while (get_bit(TWCR,TWINT)==0)
	{
	}
	
	uint8_t result = TWSR & 0b11111000;
	if (result != 0x28 && result != 0x18 && result != 0x40)
	{
		printf("transmit error: %x. ", result);
	}
}

void TWI_ReadNack (uint8_t* const ptr)
{
	TWCR = (1<<TWINT)|(1<<TWEN);

	while (get_bit(TWCR,TWINT)==0)
	{
	}

	*ptr = TWDR;
}

void TWI_ReadAck (uint8_t* const ptr)
{
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWEA);

	while (get_bit(TWCR,TWINT)==0)
	{
	}

	*ptr = TWDR;
}