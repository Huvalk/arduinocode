#include "dispatcher.hpp"
#include "definitions.hpp"
#include "usart.hpp"

// Коэф-нт делителя - тиков между задачами, зависит от шим (256, без делителя)
#define COUNT_PER_FRACTION 500 // 500 - оптимально

// счетчик
volatile uint32_t  timer0_counter = 0;
// Задача с номером
volatile uint8_t timer0_fract = 0;

// Флаги запроса обработки задачи
volatile uint8_t taskFlag = 0;

// Обработчик прерываний от счетчика 0
ISR(TIMER0_OVF_vect) {
    ++timer0_counter;

    // вызывает один раз на вызов новой задачи, нестрогое равенство на всякий случай
    if (timer0_counter < COUNT_PER_FRACTION)
    {
        return;
    } else
    {
        timer0_counter = 0;

        ++timer0_fract;

        switch (timer0_fract) {
            case 1: {
                taskFlag |= TASK_1;

                break;
            }

            case 2: {
                taskFlag |= TASK_2;

                break;
            }

            case 3: {
	            taskFlag |= TASK_3;

	            break;
            }

            default: {
                // обнуляем задачи
                timer0_fract = 0;

                break;
            }
        }
    }
}

void setupDispatcher(void) {
    // делитель таймера соответствует шим
    DDRB |= 1 << 1; // PB1 как выход, канал 1
    OCR0A = 0;
    TCCR0B |= (1 << CS00);
    TIMSK0 = (1<<TOIE0);                  // включаем прерывание при переполнении
}