#include "analizer.hpp"
#include <avr/io.h>
#include "definitions.hpp"

uint8_t pulseIntensity = 0;
uint8_t pulseStep = 5;

// разрешаем работу ADC
void setupADC() {
    // Vref=AVcc
    ADMUX |= (1<<REFS0);
    // set prescaller to 128 and enable ADC
    ADCSRA |= (1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0)|(1<<ADEN);
}

// Прочитать значение аналогового входа
uint16_t readADC(const uint8_t ADCChannel)
{
    //select ADC channel with safety mask
    ADMUX = (ADMUX & 0xF0) | (ADCChannel & 0x0F);
    //single conversion mode
    ADCSRA |= (1<<ADSC);
    // wait until ADC conversion is complete
    while( ADCSRA & (1<<ADSC) );
    return ADC;
}

void performAnalize(const uint8_t mode, const uint8_t* const colors, uint8_t* const stripes)
{
	if ((mode & OFF) || (mode == NO_ANALYZE))
	{
		return;
	}
	
	if (mode & ONE_MODE)
	{
		uint8_t intensity = volumeMeter(100);
		stripes[0] = intensity * colors[0] / 255;
		stripes[1] = intensity * colors[1] / 255;
		stripes[2] = intensity * colors[2] / 255;
	} else if (mode & THREE_MODE)
	{
		colorMusic(stripes);
	} else if (mode & PULSE_MODE)
	{
		stripes[0] = pulseIntensity * colors[0] / 255;
		stripes[1] = pulseIntensity * colors[1] / 255;
		stripes[2] = pulseIntensity * colors[2] / 255;
	asm("break");
		pulseIntensity += pulseStep;
	asm("break");
		if (pulseIntensity == 255 || pulseIntensity == 0)
		{
			pulseStep = -pulseStep;
		}
	} else if (mode & CONST_MODE)
	{
		stripes[0] = colors[0];
		stripes[1] = colors[1];
		stripes[2] = colors[2];
	}
}
