#include "analizer.hpp"
#include <avr/interrupt.h>
#include "common.hpp"
#include "definitions.hpp"
#include <math.h>

#define EXP_DEF 1.4 // степень при расчете volumeMeter
// TODO хранить в найстройках
#define LOW_PASS_DEF 310 // нижний порог срабатывания
#define SMOOTH_DEF 0.3f // сглаживание анимации
#define AVER_K_DEF 0.006f // итоговое сглаживание (aka фильтр)
#define MAX_K_DEF 1.8f //

// TODO заменить на входной параметр?
float SMOOTH_LEVEL = 0; // volumeMeter нужно хранить значение предыдущего сглаживания
float DOUBLE_SMOOTH_LEVEL = 50; // volumeMeter нужно хранить значение предыдущего анализа

uint8_t volumeMeter(const uint8_t numberOfProbes)
{
    // TODO Вынести в глобальные переменные?
    float max_level_f = 0;

    // Считывание проб
	cli();
    for (uint8_t i = 0; i < numberOfProbes; ++i)
    {
        uint16_t current_level = readADC(FIRST_CHANEL_DEF);

        // каст к инту (знаков после запятой пока нет)
        if ((int16_t)max_level_f < current_level) max_level_f = current_level;
    }
	sei();


    // Срез по нижнему порогу и перевод в 9 бит
    // TODO нужно ли?
	if (max_level_f < LOW_PASS_DEF) 
	{
		return 0;
	}
    max_level_f = map_u16t(max_level_f, LOW_PASS_DEF, 1023, 0, 255);
    if (max_level_f < 0)
    {
        max_level_f = 0;
    } else if (max_level_f > 255) {
        max_level_f = 255;
    }
	
	// остальное пока не работает
	
	return max_level_f;

    // Возведение в степень с округлением (срезаем double в float)
    max_level_f = (float)pow(max_level_f, EXP_DEF);
    // сглажанное значение от предыдущего
    SMOOTH_LEVEL = max_level_f * SMOOTH_DEF + SMOOTH_LEVEL * (1 - SMOOTH_DEF);

    // TODO нужен ли здесь порог срабатывания, если сделан срез по нижнему порогу (51)
    if (SMOOTH_LEVEL > 15)
    {
        // повторное сглажанное значение от предыдущего
        DOUBLE_SMOOTH_LEVEL = SMOOTH_LEVEL * AVER_K_DEF + DOUBLE_SMOOTH_LEVEL * (1 - AVER_K_DEF);

        // умножение на коэф-т отдельно, чтобы не влиять на DOUBLE_SMOOTH_LEVEL
		// TODO должно быть интом
        max_level_f = DOUBLE_SMOOTH_LEVEL * MAX_K_DEF;
		
        // преобразуем сигнал в шим-8
        // TODO преобразовывать нелинейно
        int16_t result_pwm_value = (SMOOTH_LEVEL - 15) * 255 / 6000;

        if (result_pwm_value < 0)
        {
            result_pwm_value = 0;
        } else if (result_pwm_value > 255) {
            result_pwm_value = 255;
        }

        return result_pwm_value;
    }

    return 0;
}