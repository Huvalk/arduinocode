#include "analizer.hpp"
#include <avr/interrupt.h>
#include "common.hpp"
#include "definitions.hpp"
#include "wff_fht/fhtConfig.hpp"
#include "wff_fht/complexToDecibel.hpp"
#include "wff_fht/windowing.hpp"
#include "usart.hpp"

#define SPEKTR_LOW_PASS 44

int16_t fx[FHT_LEN];

void processData(uint8_t* stripes)
{
	int16_t pre_stripes[3];
	pre_stripes[0] = 0;
	
	for (uint8_t i = 0 ; i < 2 ; ++i)
	{
		fx[i] -= SPEKTR_LOW_PASS;
		if (fx[i] > 0)
		{
			pre_stripes[0] += fx[i];
		}
	}
	for (uint8_t i = 2 ; i < 7 ; ++i) 
	{
		pre_stripes[0] += fx[i];
	}
	
	pre_stripes[1] = 0;
	for (uint8_t i = 8 ; i < 11 ; ++i) 
	{
		pre_stripes[1] += fx[i];
	}
	
	pre_stripes[2] = 0;
	for (uint8_t i = 11 ; i < 31 ; ++i) 
	{
		pre_stripes[2] += fx[i];
	}
	
	for (uint8_t i = 0 ; i < 3 ; ++i)
	{
		if (pre_stripes[i] > 255)
		{
			stripes[i] = 255;
		} else 
		{
			stripes[i] = (uint8_t)map_16t(pre_stripes[i], 0, 128, 0, 255);
		}
	}
}

void colorMusic(uint8_t* const stripes)
{
	cli();
	for (int i = 0 ; i < FHT_LEN ; ++i) {
		int16_t sample = readADC(FIRST_CHANEL_DEF);
		fx[i] = sample; // put real data into bins
	}
	sei();
	for (int i = 0 ; i < FHT_LEN ; ++i) {
		fx[i] = fx[i] << 4;
	}
	
	applyHammingWindow(fx);
	fhtDitInt(fx);
	complexToDecibel(fx);
	
	processData(stripes);
}