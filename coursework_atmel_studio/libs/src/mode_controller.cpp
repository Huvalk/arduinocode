#include "definitions.hpp"
#include "mode_controller.hpp"
#include "usart.hpp"
#define COLOR_STEP 15

void upColor(uint8_t* const colors);
void downColor(uint8_t* const colors);

const uint8_t STATE_MODES[12][3] = {
	{THREE_MODE, ANALYZE_MODE, NO_MODIFICATOR}, // 1
	{THREE_MODE, ANALYZE_MODE, SAVE_MOD}, // 2
	{THREE_MODE, ANALYZE_MODE, WAVE_MOD}, // 3
	{ONE_MODE, ANALYZE_MODE, NO_MODIFICATOR}, // 4
	{ONE_MODE, ANALYZE_MODE, SAVE_MOD}, // 5
	{ONE_MODE, ANALYZE_MODE, WAVE_MOD}, // 6
	{PULSE_MODE, ANALYZE_MODE, NO_MODIFICATOR}, // 7
	{PULSE_MODE, ANALYZE_MODE, WAVE_MOD}, // 8
	{CONST_MODE, ANALYZE_MODE, NO_MODIFICATOR}, // 9
	{CONST_MODE, ANALYZE_MODE, WAVE_MOD}, // 10
	{NO_ANALYZE, REPLAY_MODE, NO_MODIFICATOR}, // 11
	{NO_ANALYZE, REPLAY_MODE, WAVE_MOD} // 12
};

// 0 - ������������ ����������
// 1 - ������������ ����������, ������
// 2 - ������������ ����������, ������� �����
// 3 - ������������ ����������
// 4 - ������������ ����������, ������
// 5 - ������������ ����������, ������� �����
// 6 - ���������
// 7 - ���������, ������� �����
// 8 - ���������� ��������
// 9 - ���������� ��������, ������� �����
// 10 - ��������������� �����������
// 11 - ��������������� �����������, ������� �����
const uint8_t TRANSITION_MATRIX[12][7] = {
	{0, 0, 3, 2, 0, 0, 1}, // 0 
	{1, 1, 1, 1, 1, 1, 0}, // 1 
	{2, 2, 5, 0, 2, 2, 2}, // 2
	{3, 3, 6, 5, 3, 3, 4}, // 3
	{4, 4, 4, 4, 4, 4, 3}, // 4
	{5, 5, 7, 3, 5, 5, 5}, // 5
	{6, 6, 8, 7, 6, 6, 6}, // 6
	{7, 7, 9, 6, 7, 7, 7}, // 7
	{8, 8, 10, 9, 8, 8, 8}, // 8
	{9, 9, 11, 8, 9, 9, 9}, // 9
	{10, 10, 0, 11, 10, 10, 10}, // 10
	{11, 11, 2, 10, 11, 11, 11} // 11
};

void changeMode(const uint8_t command, uint8_t& state, uint8_t& a_mode, 
	uint8_t& s_mode, uint8_t& modificator, uint8_t* const colors,
	void (*blink)(const uint8_t), void (*saveState)(const uint8_t), void (*saveColor)(const uint8_t* const),
	void (*resetRecordPointers)(void), void (*saveRecordPointers)(void))
{
	if (command == 0)
	{
		return;
	}
	if (command == UP_BTN)
	{
		upColor(colors);
		saveColor(colors);
		return;
	} 
	if (command == DOWN_BTN)
	{
		downColor(colors);
		saveColor(colors);
		return;
	}
	
	if (command == ON_OFF)
	{
		a_mode ^= OFF;
		s_mode ^= OFF;
		modificator ^= OFF;
		
		return;
	}
	
	uint8_t old_state = state;
	state = TRANSITION_MATRIX[state][command];
	const uint8_t* const current_modes = STATE_MODES[state];
	
	if (old_state != state)
	{
		if (modificator == SAVE_MOD)
		{
			saveRecordPointers();
		}
		
		a_mode = current_modes[0];
		s_mode = current_modes[1];
		modificator = current_modes[2];
		
		if (modificator == SAVE_MOD)
		{
			resetRecordPointers();
		}
		
		saveState(state);
		blink(state);
	}
}


void breakRecording(const uint8_t command, uint8_t& state, uint8_t& a_mode,
	uint8_t& s_mode, uint8_t& modificator,
	void (*blink)(const uint8_t), void (*saveState)(const uint8_t), void (*saveRecordPointers)(void))
{
	if (STATE_MODES[state][2] != SAVE_MOD)
	{
		return;
	}
	
	--state;
	const uint8_t* const current_modes = STATE_MODES[state];

	a_mode = current_modes[0];
	s_mode = current_modes[1];
	modificator = current_modes[2];
	
	saveRecordPointers();
	saveState(state);
	blink(state);
	blink(state);
	blink(state);
}

void upColor(uint8_t* const colors)
{
	if (colors[0] == 255 && colors[1] != 255 && colors[2] == 0)
	{
		colors[1] += COLOR_STEP;
		return;
	}
	
	if (colors[0] != 0 && colors[1] == 255 && colors[2] == 0)
	{		
		colors[0] -= COLOR_STEP;
		return;
	}

	if (colors[0] == 0 && colors[1] == 255 && colors[2] != 255)
	{
		colors[2] += COLOR_STEP;
		return;
	}

	if (colors[0] == 0 && colors[1] != 0 && colors[2] == 255)
	{
		colors[1] -= COLOR_STEP;
		return;
	}
	
	if (colors[0] != 255 && colors[1] == 0 && colors[2] == 255)
	{
		colors[0] += COLOR_STEP;
		return;
	}
	
	if (colors[0] == 255 && colors[1] == 0 && colors[2] != 0)
	{
		colors[2] -= COLOR_STEP;
		return;
	}
}

void downColor(uint8_t* const colors)
{
	if (colors[0] == 255 && colors[1] != 0 && colors[2] == 0)
	{
		colors[1] -= COLOR_STEP;
		return;
	}
	
	if (colors[0] != 255 && colors[1] == 255 && colors[2] == 0)
	{
		colors[0] += COLOR_STEP;
		return;
	}

	if (colors[0] == 0 && colors[1] == 255 && colors[2] != 0)
	{
		colors[2] -= COLOR_STEP;
		return;
	}

	if (colors[0] == 0 && colors[1] != 255 && colors[2] == 255)
	{
		colors[1] += COLOR_STEP;
		return;
	}
	
	if (colors[0] != 0 && colors[1] == 0 && colors[2] == 255)
	{
		colors[0] -= COLOR_STEP;
		return;
	}
	
	if (colors[0] == 255 && colors[1] == 0 && colors[2] != 255)
	{
		colors[2] += COLOR_STEP;
		return;
	}
}

void restoreModes(uint8_t& state, uint8_t& a_mode, uint8_t& s_mode, uint8_t& modificator)
{
	if (STATE_MODES[state][2] == SAVE_MOD)
	{
		--state;
		const uint8_t* const current_modes = STATE_MODES[state];
		
		a_mode = current_modes[0];
		s_mode = current_modes[1];
		modificator = current_modes[2];
		return;
	}

	const uint8_t* const current_modes = STATE_MODES[state];
	
	a_mode = current_modes[0];
	s_mode = current_modes[1];
	modificator = current_modes[2];
}