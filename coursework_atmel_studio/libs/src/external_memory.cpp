#include "memory.hpp"
#include "twi.hpp"
#include "usart.hpp"

#define EXTERNAL_MEMORY_SIZE 0x7FFF
// Memory addresses
#define RECORD_ENDS_ADDRESS 0x0000
#define MUSIC_STARTS_FROM 0x0002

uint16_t playPointer = MUSIC_STARTS_FROM;
uint16_t recordsEnds = MUSIC_STARTS_FROM;

void EEPROM_ExternalRead(const uint16_t address, uint8_t* const ptr, const uint8_t size);
void EEPROM_ExternalWrite(const uint8_t* const data, const uint8_t size, const uint16_t address);

void setupExternalMemory()
{
	TWI_Init();
	restoreRecordPointers();
}

bool saveSoundNext(const uint8_t* const stripes)
{
	if (recordsEnds < (EXTERNAL_MEMORY_SIZE - 2))
	{
		EEPROM_ExternalWrite(stripes, 3, recordsEnds);
		recordsEnds += 3;
		return true;
	}
	
	return false;
}

void resetRecordPointers()
{
	recordsEnds = MUSIC_STARTS_FROM;
}

void saveRecordEnds()
{
	EEPROM_ExternalWrite((uint8_t *)&recordsEnds, 2, RECORD_ENDS_ADDRESS);\
}

void play(uint8_t* const stripes)
{
	if (playPointer < (recordsEnds))
	{
		EEPROM_ExternalRead(playPointer, stripes, 3);
		playPointer += 3;
		return;
	}
	
	playPointer = MUSIC_STARTS_FROM;
}

void restoreRecordPointers()
{
	EEPROM_ExternalRead(RECORD_ENDS_ADDRESS, (uint8_t *)&recordsEnds, 2);
	
	if (recordsEnds >= EXTERNAL_MEMORY_SIZE)
	{
		recordsEnds = MUSIC_STARTS_FROM;
	}
}

void EEPROM_ExternalWrite(const uint8_t* const data, const uint8_t size, const uint16_t address)
{
	TWI_Start();
	TWI_Write(0xA0); // 1010 - ����������� �����, 000 - CS, 0 - ������
	TWI_Write(hi(address));
	TWI_Write(low(address));
	for (uint8_t i = 0; i < size; ++i)
	{
		TWI_Write(data[i]);
	}
	TWI_Stop();
}

void EEPROM_ExternalRead(const uint16_t address, uint8_t* const ptr, const uint8_t size)
{
	TWI_Start();
	TWI_Write(0xA0);
	TWI_Write(hi(address));
	TWI_Write(low(address));
	TWI_Start();
	TWI_Write(0xA1); // 1010 - ����������� �����, 000 - CS, 1 - ������
	for (uint8_t i = 0; i < (size - 1); ++i)
	{
		TWI_ReadAck(ptr + i);
	}
	TWI_ReadNack(ptr + size - 1);
	TWI_Stop();
}
