#include <avr/delay.h>
#include <avr/io.h>
#include "definitions.hpp"
#include "memory.hpp"
#include "s_to_p.hpp"
#include "usart.hpp"

#define MAX_LINES 4
#define COUNTER_TOP 20
uint8_t currentLine = 0;
uint8_t counter = 0;
uint8_t previousModificator = NO_MODIFICATOR;

// Инициализация всех нужных шим модулей
void setupPWM() 
{
    DDRB |= 1 << 1; // PB1 как выход, канал 1
    DDRB |= 1 << 3; // PB3 как выход, канал 2
    DDRD |= 1 << 6; // PD6 как выход, канал 3

    // Задать вывод и режим работы
    TCCR0A |= (1 << WGM00) | (1 << COM0A1);
    TCCR1A |= (1 << WGM10) | (1 << COM1A1);
    TCCR2A |= (1 << WGM20) | (1 << COM2A1);
    // Работа без делителя
    TCCR0B |= (1 << CS00);
    TCCR1B |= (1 << CS10);
    TCCR2B |= (1 << CS20);

	setupRegister();
}

void blink(const uint8_t num)
{
	for (uint8_t i = 0; i < num; ++i)
	{
		if ((i & 1) || (i & 8))
		{
			OCR0A = 255;
			OCR1A = OCR2A = 0;
			_delay_ms(500);
		}
		
		if ((i & 2) || (i & 8))
		{
			OCR1A = 255;
			OCR0A = OCR2A = 0;
			_delay_ms(500);
		}
		
		if ((i & 4) || (i & 8))
		{
			OCR2A = 255;
			OCR0A = OCR1A = 0;
			_delay_ms(500);
		}
	}
	
	OCR0A = OCR1A = OCR2A = 0;
}

void performVisualize(const uint8_t mode, const uint8_t modificator, 
	uint8_t* const colors, void (*play)(uint8_t* const))
{
	if ((mode & OFF) || (mode == NO_SHOW))
	{
		return;
	}
	
	if (mode & ANALYZE_MODE)
	{
		OCR0A = colors[0];
		OCR1A = colors[1];
		OCR2A = colors[2];
	} else if (mode & REPLAY_MODE)
	{
		play(colors);
		OCR0A = colors[0];
		OCR1A = colors[1];
		OCR2A = colors[2];
	}
	
	if (modificator == WAVE_MOD)
	{
		if (modificator != previousModificator)
		{
			writeOne();
			currentLine = 0;
			counter = 0;
		}
		
		if (counter > COUNTER_TOP)
		{
			++currentLine;
			if (currentLine < MAX_LINES)
			{
				shift();
			} else 
			{
				writeOne();
				currentLine = 0;
			}
			
			counter = 0;
		} else 
		{
			++counter;
		}
	} else if (previousModificator == WAVE_MOD)
	{
		set();
	}
	
	previousModificator = modificator;
}