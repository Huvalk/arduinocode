#include "wff_fht/fhtConfig.hpp"
#include "usart.hpp"

#define UART_BAUD_RATE  38400
#define UART_BAUD_REGISTERS  (16000000UL / 16 / UART_BAUD_RATE - 1)

FILE * uart_str;

int printChar(char character, FILE *stream)
{
	while ((UCSR0A & (1 << UDRE0)) == 0);

	UDR0 = character;

	return 0;
}

int getChar(FILE *stream)
{
	while ( !(UCSR0A & (1<<RXC0)) )
	{}
		
	char data = UDR0;
	if(data == '\r')
	data = '\n';
	printChar(data, stream); 
	return data;
}

void setupUsart()
{
	UBRR0H = UART_BAUD_REGISTERS >> 8;
	UBRR0L = UART_BAUD_REGISTERS;

	UCSR0B = (1 << RXEN0) | (1 << TXEN0);
	uart_str = fdevopen(printChar, getChar);
	stdout = uart_str;
}
