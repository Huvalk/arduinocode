#ifndef MODE_CONTROLLER_HPP
#define MODE_CONTROLLER_HPP

#include <stdint.h>

void changeMode(const uint8_t command, uint8_t& state, uint8_t& a_mode, 
	uint8_t& s_mode, uint8_t& modificator, uint8_t* const colors,
	void (*blink)(const uint8_t), void (*saveState)(const uint8_t), void (*saveColor)(const uint8_t* const),
	void (*resetRecordPointers)(void), void (*saveRecordPointers)(void));
void breakRecording(uint8_t& state, uint8_t& a_mode, uint8_t& s_mode, uint8_t& modificator,
	void (*blink)(const uint8_t), void (*saveState)(const uint8_t), void (*saveRecordPointers)(void));
void restoreModes(uint8_t& state, uint8_t& a_mode, uint8_t& s_mode, uint8_t& modificator);

#endif /* MODE_CONTROLLER_HPP */