#ifndef USART_HPP
#define USART_HPP

#include <stdio.h>

// External function prototypes
int printChar(char character, FILE *stream);
void setupUsart();

#endif /* USART_HPP */