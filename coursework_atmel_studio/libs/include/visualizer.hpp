#ifndef MPS_VISUALIZER_HPP
#define MPS_VISUALIZER_HPP

void setupPWM();
void blink(const uint8_t num);
void performVisualize(const uint8_t mode, const uint8_t modificator, 
	uint8_t* const colors, void (*play)(uint8_t* const));

#endif //MPS_VISUALIZER_HPP
