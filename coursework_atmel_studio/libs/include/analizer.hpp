#ifndef MPS_ANALIZER_HPP
#define MPS_ANALIZER_HPP

#include <inttypes.h>

void setupADC(void);
uint16_t readADC(const uint8_t ADCChannel);
void performAnalize(const uint8_t mode, const uint8_t* const colors, uint8_t* const stripes);

// �������� ������� �� ������� ���������
uint8_t volumeMeter(const uint8_t numberOfProbes);
void colorMusic(uint8_t* const stripes);

#endif //MPS_ANALIZER_HPP
