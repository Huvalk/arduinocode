#ifndef MPS_COMMON_HPP
#define MPS_COMMON_HPP

#include <inttypes.h>

uint16_t map_u16t(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min, uint16_t out_max);
uint16_t map_16t(int16_t x, int16_t in_min, int16_t in_max, int16_t out_min, int16_t out_max);

#endif //MPS_COMMON_HPP
