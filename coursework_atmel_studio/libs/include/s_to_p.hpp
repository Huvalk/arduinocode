#ifndef S_TO_P_HPP
#define S_TO_P_HPP

void setupRegister();
void writeOne();
void shift();
void clear();
void set();

#endif /* S_TO_P_HPP */