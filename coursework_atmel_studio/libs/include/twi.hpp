#ifndef TWI_HPP
#define TWI_HPP

#include <stdint.h>

#define hi(n) (n >> 8)
#define low(n) (n & 0xFF)

void TWI_Init();
void TWI_Start();
void TWI_Stop();
void TWI_Write(const uint8_t data);
void TWI_ReadNack (uint8_t* const ptr);
void TWI_ReadAck (uint8_t* const ptr);

#endif /* TWI_HPP */