#ifndef MEMORY_HPP
#define MEMORY_HPP

#include <stdint.h>

// Addresses in internal eeprom
void setupExternalMemory();

void saveState(const uint8_t state);
void saveColor(const uint8_t* const color);
void restoreSettings(uint8_t& state, uint8_t* const color);

bool saveSoundNext(const uint8_t* const stripes);
void restoreRecordPointers();
void resetRecordPointers();
void saveRecordEnds();
void play(uint8_t* const stripes);

#endif /* MEMORY_HPP */