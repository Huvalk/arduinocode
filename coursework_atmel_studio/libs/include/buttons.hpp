#ifndef BUTTONS_HPP
#define BUTTONS_HPP

void setupButtons();
uint8_t processButtons();

#endif /* BUTTONS_HPP */