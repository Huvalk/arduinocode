#ifndef MPS_DISPATCHER_HPP
#define MPS_DISPATCHER_HPP

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <inttypes.h>

void setupDispatcher(void);

#endif //MPS_DISPATCHER_HPP
