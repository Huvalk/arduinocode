#define F_CPU 16000000UL
#include "analizer.hpp"
#include <avr/io.h>
#include <avr/interrupt.h>
#include "buttons.hpp"
#include "definitions.hpp"
#include "dispatcher.hpp"
#include "mode_controller.hpp"
#include "memory.hpp"
#include "visualizer.hpp"
#include <stdio.h>
#include "usart.hpp"
#include <util/delay.h>

// Флаги запроса обработки задачи
extern volatile uint8_t taskFlag;

// Инициализация начального состояния
void setupMain()
{
	setupDispatcher();
	setupPWM();
	setupADC();
	setupUsart();
	setupButtons();
	setupExternalMemory();
}

// ********************************************************************************
// Main
// ********************************************************************************
int main( void ) 
{
	setupMain();
	uint8_t colors[3] = {0, 0, 0};
	uint8_t state, a_mode, s_mode, modificator;
	restoreSettings(state, colors);
	restoreModes(state, a_mode, s_mode, modificator);
	bool continueSaving = true;
	
	sei();
	
    while(true) {
        if (taskFlag & TASK_1) {
	        uint8_t stripes[3] = {0, 0, 0};
			
			performAnalize(a_mode, colors, stripes);
			performVisualize(s_mode, modificator, stripes, play);
			if (modificator == SAVE_MOD)
			{
				saveSoundNext(stripes);
			}
			
            taskFlag &= ~TASK_1;
        }
		
		if (taskFlag & TASK_2) {
			if (continueSaving == true)
			{
				uint8_t command = processButtons();
				changeMode(command, state, a_mode, s_mode, modificator, colors,
					&blink, &saveState, &saveColor, &resetRecordPointers, &saveRecordEnds);
				//TODO отдельная функция для сохранения
				/*printf("state - % i, modificator - % i\n", state, modificator);*/
			} else 
			{
				breakRecording(state, a_mode, s_mode, modificator,
					&blink, &saveState, &saveRecordEnds);
			}

			taskFlag &= ~TASK_2;
		}
    }
}




